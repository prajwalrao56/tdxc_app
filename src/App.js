import React, { useState, useRef, useReducer } from "react";
import * as mobilenet from "@tensorflow-models/mobilenet";
import "./App.css";

// import camera component
import { CameraFeed } from './components/camera-feed';
const uploadImage = async file => {
  const formData = new FormData();
  formData.append('file', file);


};
// flow for loading model -> uploading img -> identifying img -> reset
const machine = {
  initial: "initial",
  states: {
    initial: { on: { next: "loadingModel" } },
    loadingModel: { on: { next: "modelReady" } },
    modelReady: { on: { next: "imageReady" } },
    imageReady: { on: { next: "identifying" }, showImage: true },
    identifying: { on: { next: "complete" } },
    complete: { on: { next: "modelReady" }, showImage: true, showResults: true }
  }
};

function App() {


  const [results, setResults] = useState([]);
  const [imageURL, setImageURL] = useState(null);
  const [model, setModel] = useState(null);
  const imageRef = useRef();
  const inputRef = useRef();


  const reducer = (state, event) =>
    machine.states[state].on[event] || machine.initial;

  const [appState, dispatch] = useReducer(reducer, machine.initial);
  const next = () => dispatch("next");

  const loadModel = async () => {
    next();
    const model = await mobilenet.load();
    setModel(model);
    next();
  };

  const identify = async () => {
    next();
    const results = await model.classify(imageRef.current);
    setResults(results);
    next();
  };

  const reset = async () => {
    setResults([]);
    next();
  };

  const upload = () => inputRef.current.click();

  const handleUpload = event => {
    const { files } = event.target;
    if (files.length > 0) {
      // resizing images to more fixed size 600*600
      var resize_width = 600;
      var img = new Image();//create a image
      img.src = URL.createObjectURL(event.target.files[0]);//result is base64-encoded Data URI
      img.name = event.target.files[0].name;//set name (optional)
      img.size = event.target.files[0].size;//set size (optional)
      console.log("resize start")
      console.log(img)
      img.onload = function (el) {
        console.log("resize start in if ")
        var elem = document.createElement('canvas');//create a canvas

        //scale the image to 600 (width) and keep aspect ratio
        var scaleFactor = resize_width / el.target.width;
        elem.width = resize_width;
        elem.height = el.target.height * scaleFactor;

        //draw in canvas
        var ctx = elem.getContext('2d');
        ctx.drawImage(el.target, 0, 0, elem.width, elem.height);

        //get the base64-encoded Data URI from the resize image
        var srcEncoded = ctx.canvas.toDataURL(el.target, 'image/jpeg', 0);

        //assign it to thumb src
        //document.querySelector('#image').src = srcEncoded;

        //const url = URL.createObjectURL(event.target.files[0]);
        setImageURL(srcEncoded);
        
        next();
      }
    }
  };

const actionButton = {
  initial: { action: loadModel, text: "Load Model" },
  loadingModel: { text: "Loading Model..." },
  modelReady: { action: upload, text: "Upload Image" },
  imageReady: { action: identify, text: "Identify Fruit" },
  identifying: { text: "Identifying..." },
  complete: { action: reset, text: "Reset" }
};

const { showImage, showResults } = machine.states[appState];

return (
  <div>
    <h1 > Classify Image project by prajwal</h1>



    <div>
      <div>
        <h2 > Classify Image by upload</h2>
      </div>
      {showImage && <img crossOrigin='anonymous' id="modelpred1" src={imageURL} alt="upload-preview" ref={imageRef} />}
      <input
        type="file"
        accept="image/*"
        capture="camera"
        onChange={handleUpload}
        ref={inputRef}
      />
      {showResults && (
        <ul>
          {results.map(({ className, probability }) => (
            <li key={className}>{`${className}: %${(probability * 100).toFixed(
              2
            )}`}</li>
          ))}
        </ul>
      )}
      <button onClick={actionButton[appState].action || (() => { })}>
        {actionButton[appState].text}
      </button>

    </div>

    <div>
      <div>

      </div>
      <h2 > Classify Image from camera</h2>
    </div>
    <div>
      <h2>Image capture from camera</h2>
      <p>Capture image from  webcamera and classify objects in the image</p>
      <CameraFeed sendFile={uploadImage} />
    </div>
  </div>
);
  }

export default App;
