import React, { Component } from 'react';
import * as mobilenet from "@tensorflow-models/mobilenet";
import ReactDOM from 'react-dom';


export class CameraFeed extends Component {


    /**
     * Processes available devices and identifies one by the label
     * @memberof CameraFeed
     * @instance
     */

    processDevices(devices) {
        devices.forEach(device => {
            console.log(device.label);
            this.setDevice(device);
        });
    }

    /**
     * Sets the active device and starts playing the feed
     * @memberof CameraFeed
     * @instance
     */
    async setDevice(device) {
        const { deviceId } = device;
        const stream = await navigator.mediaDevices.getUserMedia({ audio: false, video: { deviceId } });
        this.videoPlayer.srcObject = stream;
        this.videoPlayer.play();

    }





    /**
     * On mount, grab the users connected devices and process them
     * @memberof CameraFeed
     * @instance
     * @override
     */
    async componentDidMount() {
        const cameras = await navigator.mediaDevices.enumerateDevices();
        this.processDevices(cameras);
    }

    /**
     * Handles taking a still image from the video feed on the camera
     * @memberof CameraFeed
     * @instance
     */
    takePhoto = async () => {
        this.result = null


        const { sendFile } = this.props;
        const context = this.canvas.getContext('2d');
        context.drawImage(this.videoPlayer, 0, 0, 224, 224);
        this.canvas.toBlob(sendFile);



        //Loads mobile net model and calls the classify method woth the video player image

        const model = await mobilenet.load();

        const results = await model.classify(this.videoPlayer);
        console.log(results)
        this.results = results
        this.showResults = true

        // element to display classification results

        const ele = (
            <ul>
                {results.map(({ className, probability }) => (
                    <li key={className}>{`${className}: %${(probability * 100).toFixed(
                        2
                    )}`}</li>
                ))}
            </ul>
        )

        ReactDOM.render(ele, document.getElementById('modelresult'));
    };

    render() {
        return (
            <div className="c-camera-feed">
                <div >
                    <div className="c-camera-feed__viewer">
                        <video ref={ref => (this.videoPlayer = ref)} />
                    </div>
                    <button onClick={this.takePhoto}>Take and classify photo!</button>
                    <div className="c-camera-feed__stage">
                        <canvas id="modelpred" ref={ref => (this.canvas = ref)} />
                    </div>
                </div>
                <div id="modelresult">
                </div>
            </div>
        );
    }
}
