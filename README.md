Author : Prajwal

This Project was build useing react , tensorflow.js and mobilenet model

steps taken :

- create new app with create react command 
- integrate tensorflow.js to import mobilenet 
- create ui element to give users ability to upload images and get classification results with probability scores
- Use pretrained mobilenet to classify fruits and objects
- create new camera-feed component to capture and classify images from camera

further advancements needed
- retrain mobilenet to increase prediction accuracy use tool like make_image_classifier that comes with the tensorflow_hub library(https://github.com/tensorflow/hub/tree/master/tensorflow_hub/tools/make_image_classifier)




## Available Scripts

In the project directory, you can run:

# run npm install first
### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

